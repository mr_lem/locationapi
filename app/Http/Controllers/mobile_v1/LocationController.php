<?php

namespace App\Http\Controllers\mobile_v1;

use Response;
use Illuminate\Http\Request;
use Geocode;

use App\Http\Requests;
use Input;
use App\Http\Controllers\Controller;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        echo('index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    public function getaddress(){

        /*Get the user input latittude/longitude*/

        $latitude = Input::get('latitude');
        $longitude = Input::get('longitude');

        // $latitude = '14.572453';
        // $longitude = '121.080414';

        /*parse address using this lubrary*/
        $address = Geocode::make()->latLng($latitude,$longitude);
        if ($address) {
            $response = [
                'result' => 'success',
                'address'   =>  $address,
            ];
            return Response::json($response);
        }else{
            $response = [
                'result' => 'false'
            ];
            return Response::json($response);
        }
    }

   /* public function getaddress(){
        // print_r($_GET);
        // print_r(Input::all());
        $response = [
                'result' => 'success234'
            ];
            return Response::json($response);
    }*/
}
