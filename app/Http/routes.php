<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(array('prefix' => 'api/v1'),function(){
	Route::resource('location', 'mobile_v1\LocationController'); //works on GET

	// Route::post('/address/postaddress', 'mobile_v1\LocationController@postaddress');

  Route::get('/address', 'mobile_v1\LocationController@getaddress'); //works on GET

});
